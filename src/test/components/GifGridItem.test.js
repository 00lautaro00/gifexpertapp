import { shallow } from "enzyme"
import React from 'react';
import { GifGridItem } from '../../components/GifGridItem'


describe('Pruebas en gifgriditem', () => {
    const url = 'htpps//:localhost/otracosa';
    const title= 'hola.aloh';
    const wrapper = shallow( < GifGridItem url={url} title={title} / > );

    test('Probando el componente', () => {
       

        expect(wrapper).toMatchSnapshot();

    })

        test('debe tener un parafo con el title', () => {

        const p = wrapper.find('p')
        expect(p.text().trim()).toBe( title );

        
    })
        test('debe tener img igual al url', () => {

           const img = wrapper.find('img')
           expect(img.prop('src')).toBe( url );
           expect(img.prop('alt')).toBe( title );


        
    })

    test('probando clases', () => {
        
        const div = wrapper.find('div')
        const className = div.prop('className')
        expect(className.includes('animate__fadeIn')).toBe(true)

                
    })
    



    
})