import React, { useState } from 'react';
import { AddCategory } from './components/AddCategory';
import { GifGrid } from './components/GifGrid';


export const GifExpertApp = ({gif}) => {
    const [categories, setCategories] = useState(['One Punch'])

    // const handleAdd = () => {
        
    //     // setCategories([...categories, 'HUNTERXHUNTER'])
    // setCategories(cats =>  [...cats, e])
        

    // }

    // const categories = ['One Punch', 'Samurai X', 'Dragon Ball'];
    return(
      <>
      <h2 className="text animate__animated animate__bounce">{gif}</h2>
      <AddCategory setCategories={setCategories} />
      <hr/>
     <ol>
         { 
             categories.map( category => 
                
                 <GifGrid
                 key={category}
                  category={category}
                   /> 
                //  return <li key={ category }>{category}</li>
             )
         }
     </ol>
      
      </>
    )
    }

export default GifExpertApp;

