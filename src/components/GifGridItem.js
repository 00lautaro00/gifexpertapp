import PropTypes from 'prop-types';
import React from 'react'



export const GifGridItem = ({ title, url }) => {
    return ( 
        <div className = "card animate__animated animate__fadeIn animate__slower" >
        <img src = { url }alt = { title }/>
         <p> { title } </p >


        </div>
    )
}

GifGridItem.propTypes = {
    url: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired

}